module TrackIp
	IP_ADDRESS_REGEX = /^((?:(?:^|\.)(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])){4})$/.freeze
	IpAddress = Struct.new(:ip)
	TopAddress = Struct.new(:ip, :count)
	@ips = {}
	@top_ips = []
	LIMIT = 100

	def request_handled(ip_address)
		if is_valid_ip_address?(ip_address)
			count = add_count(ip_address) # count +1
			sort_top(ip_address, count)
		end
	end

	def top100
		@top_ips
	end

	def clear
		@ips = {}
		@top_ips = []
	end

	def is_valid_ip_address?(ip)
		ip =~ IP_ADDRESS_REGEX
	end

	def add_count(ip_address)
		ip = IpAddress.new(ip_address)
		current_count = @ips[ip].nil? ? 1 : @ips[ip] + 1
		@ips[ip] = current_count
		current_count
	end

	def sort_top(ip_address, count)
		repeated = @top_ips.find { |a| a.ip == ip_address }
		if repeated.nil?
			@top_ips << TopAddress.new(ip_address, count)
		else
			repeated.count = count
		end
		# sort and get top 100 by count
		@top_ips.sort_by! { |k| k['count'] }.reverse!
		@top_ips = @top_ips[0..LIMIT - 1]
	end
end
 
# Execution
include TrackIp
10_000.times.each do |_val|
	TrackIp.request_handled('145.87.2.108')
	TrackIp.request_handled('145.87.2.110')
	TrackIp.request_handled('145.87.2.109')
	TrackIp.request_handled('145.87.2.111')
	TrackIp.request_handled('145.87.2.112')
	TrackIp.request_handled('145.87.2.109')
	TrackIp.request_handled('145.87.2.113')
	TrackIp.request_handled('145.87.2.109')
	TrackIp.request_handled('145.87.2.108')
	TrackIp.request_handled('145.87.2.114')
	TrackIp.request_handled('145.87.2.115')
	TrackIp.request_handled('145.87.2.116')
	TrackIp.request_handled('145.87.2.117')
	TrackIp.request_handled('145.87.2.118')
	TrackIp.request_handled('145.87.2.114')
	TrackIp.request_handled('145.87.2.114')
	TrackIp.request_handled('145.87.2.114')
	TrackIp.request_handled('145.87.2.108')
	TrackIp.request_handled('145.87.2.108')
	TrackIp.request_handled('145.87.2.109')
	TrackIp.request_handled('145.87.2.108')
	TrackIp.request_handled('145.87.2.113')
end

p TrackIp.top100
TrackIp.clear
p TrackIp.top100