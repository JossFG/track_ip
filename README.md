# Answers

## What‌ ‌would‌ ‌you‌ ‌do‌ ‌differently‌ ‌if‌ ‌you‌ ‌had‌ ‌more‌ ‌time?‌
Use redis to store data in cache and make operations with better performance.

## What‌ ‌is‌ ‌the‌ ‌runtime‌ ‌complexity‌ ‌of‌ ‌each‌ ‌function?‌ ‌
#### request_handled(ip_address): O(n).

#### top100()‌: O(1)

#### clear(): O(1)

## How‌ ‌does‌ ‌your‌ ‌code‌ ‌work?‌ ‌
1. First have an structure for ip address and top ip address.
2. Check if ip address received is valid.
3. Create a new instance structure using the ip address.
3. Validate if ip address exist in array of ips and increments by one the field count otherwise add to the ips array with count equal to one.
4. Sort top ip array from max to min and only get the values indicated by the limit in this case 100 elements.
5. To get top 100 we only need to send the top array.
6. Clear the array of ips and top ips using the method clear(). 

## What‌ ‌other‌ ‌approaches‌ ‌did‌ ‌you‌ ‌decide‌ ‌not‌ ‌to‌ ‌pursue?‌ ‌
Another alternative maybe could be to sort the elements using only one array but this will have a lot of data so it will affect to the performance of the
application. I decided to get the top 100 using another array to order every time that we receive a new ip address.

## How‌ ‌would‌ ‌you‌ ‌test‌ ‌this?‌
I would test this program using Rspec, that allows to create unit tests to handle a lot of data to test the funcionality of every function.